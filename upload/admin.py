from django.contrib import admin
from .models import Upload

# Register your models here and add to admin interface
admin.site.register(Upload)
