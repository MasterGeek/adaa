from django.db import models

# Create your models here.

class Upload(models.Model):
    title = models.CharField(max_length=200)
    file = models.FileField()

    class Meta:
        db_table = "csv"

    def __str__(self):
        return 'Title of this file is :' + self.title

