from django.db import models
from django.conf import settings

# Create your models here.
class PubliChatRoom(models.Model):
    title = models.CharField(max_length=255,unique=True , blank=False)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL,   help_text = "users who  are connected to the chat")

    def __str__(self):
        return self.title

    def connect_user(self, user):
        is_user_added = False
        if not user in self.users.all():
            self.users.add(user)
            self.save()
            is_user_added = True
        elif user in self.users.all():
            is_user_added =True
        return is_user_added

    def disconnect_user(self, user):
        is_user_connected = False
        if user in self.users.all():
            self.users.remove(user)
            self.save()
            is_user_connected = True
        return is_user_connected

    @property
    def group_name(self):
        return f'PubliChatRoom-{self.id}'

class PublicRoomChatMessageManager(models.Model):
    def by_room(self,room):
        query = PublicRooomChatMessage.objects.filter(room=room).order_by('-timestamp')  
        return query 


class PublicRooomChatMessage(models.Model):
    user= models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    room = models.ForeignKey(PubliChatRoom, on_delete=models.CASCADE)
    timestamp = models.DateField(auto_now_add=False)
    content = models.TextField(unique=False, blank=False)

    def __str__(self):
        return self.content
    




    








    


    

