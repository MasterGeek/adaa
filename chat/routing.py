from django.urls import re_path

from . import consummers
websocket_urslPatterns = [
    re_path(r'ws/chat/(?P<room_name>\w+)/$' , consummers.ChatConsumer.as_asgi()),  #aasgi() call the  ASGI Applicatio to intantiate a consummer
]