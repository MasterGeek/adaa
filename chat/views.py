from django.shortcuts import render

# Create your views here.
def index(request):
    # render the home page of our app 
    return render(request, 'chat/index.html') 
    
def room(request, room_name):
    return render(request, 'chat/room.html' , {
        'room_name': room_name
    })

